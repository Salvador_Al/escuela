import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// rutas de las paginas de nuestra aplicacion
import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { InicioComponent } from './Components/inicio/inicio.component';
import { NavBarComponent } from './Components/Shared/nav-bar/nav-bar.component';
import { AlumnosService } from './Services/Alumnos.service';
import { MateriasService } from './Services/Materias.service';
import { AlumnosComponent } from './Components/alumnos/alumnos.component';
import { MateriasComponent } from './Components/materias/materias.component';
import { PromediosComponent } from './Components/promedios/promedios.component';
import { FooterComponent } from './Components/Shared/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NavBarComponent,
    AlumnosComponent,
    MateriasComponent,
    PromediosComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [
    AlumnosService,
    MateriasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
