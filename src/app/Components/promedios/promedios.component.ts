import { Component, OnInit } from '@angular/core';
import { AlumnosService, Alu } from '../../Services/Alumnos.service';
import { MateriasService, Mat } from '../../Services/Materias.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-promedios',
  templateUrl: './promedios.component.html',
  styles: []
})
export class PromediosComponent implements OnInit {
  NumCtr: any = {};
  promgen = 0;
  MM: any = {};
  tam: number;
  clvmateri: any = {};
  mate: any = {};
  prom: any;
  // tslint:disable-next-line:no-shadowed-variable
  constructor(private activateRouter: ActivatedRoute, private AlumnosService: AlumnosService, private MateriasService: MateriasService ) {
    this.activateRouter.params.subscribe(params => {this.NumCtr = this.AlumnosService.getNuCtrl(params[ 'con' ] );
  });
    this.clvmateri = this.NumCtr.c_materia;
    console.log('alumno: ', this.NumCtr);
    this.tam = this.clvmateri.length;
    console.log('Tamaño: ', this.tam);
    console.log(this.clvmateri);
    this.mate = new Array(Number(this.tam));
    this.MM = this.MateriasService.getMat();
    let nom: any;
    let clv: any;
    let uni: any;
    let pro: any;
    let x = 0;
    let prosum = 0;
              // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.clvmateri.length; i++) {
              // tslint:disable-next-line:prefer-for-of
      for (let j = 0; j < this.MM.length; j++) {
      if (this.MM[j].clv_mat === this.clvmateri[i]) {
        this.prom = Math.floor(Math.random() * (100 - 50)) + 50;
        nom = this.MM[j].nom_mat;
        clv = this.MM[j].clv_mat;
        uni = this.MM[j].no_uni;
        pro = this.prom;
        // tslint:disable-next-line:radix
        prosum += parseInt(pro);
        console.log('prosum:' , prosum);
        // tslint:disable-next-line:radix
        console.log('uni:' , uni);
        console.log('progen:' , this.promgen);
        this.mate[i] = {nombre: nom, clve: clv, unid: uni, pr: pro};
        x++;
      }
      }
    }
    this.promgen = prosum / x;
    console.log('Materia', this.mate);
  }
  ngOnInit() {
  }
}
