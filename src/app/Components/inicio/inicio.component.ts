import { Component, OnInit } from '@angular/core';
import { AlumnosService, Alu } from '../../Services/Alumnos.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styles: []
})
export class InicioComponent implements OnInit {
  Alumno: Alu[] = [];
  constructor( private alumnosService: AlumnosService, private router: Router) {
   }

  ngOnInit(): void {
    this.Alumno = this.alumnosService.getAlu();
  }

  Promedio(con: number) {
    this.router.navigate(['/Promedios', con]);
    console.log('Control: ', con);
  }
}
