import { Component, OnInit } from '@angular/core';
import { AlumnosService, Alu } from '../../Services/Alumnos.service';
import { Router, Routes } from '@angular/router';

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styles: []
})
export class AlumnosComponent implements OnInit {
  Alumno: Alu[] = [];
  constructor( private alumnosService: AlumnosService, private router: Router) { }

  ngOnInit(): void { 
    this.Alumno = this.alumnosService.getAlu();
  }

}
