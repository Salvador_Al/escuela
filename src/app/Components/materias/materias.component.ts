import { Component, OnInit } from '@angular/core';
import { MateriasService, Mat } from '../../Services/Materias.service';
import { Router, Routes } from '@angular/router';

@Component({
  selector: 'app-materias',
  templateUrl: './materias.component.html',
  styles: []
})
export class MateriasComponent implements OnInit {
  Mate: Mat[] = [];
  constructor( private materiasService: MateriasService, private router: Router) { }

  ngOnInit(): void { 
    this.Mate = this.materiasService.getMat();
  }

}
