import { RouterModule, Routes} from '@angular/router' ;
import { APPLICATION_MODULE_PROVIDERS } from '@angular/core/src/application_module';
import { InicioComponent } from './Components/inicio/inicio.component';
import { AlumnosComponent } from './Components/alumnos/alumnos.component';
import { MateriasComponent } from './Components/materias/materias.component';
import { PromediosComponent } from './Components/promedios/promedios.component';

const APP_ROUTES: Routes = [
    {path: 'Inicio', component: InicioComponent},
    {path: 'Alumnos', component: AlumnosComponent},
    {path: 'Materias', component: MateriasComponent},
    {path: 'Promedios/:con', component: PromediosComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'nombre_ruta'}
    //{path: 'Inicio/Promedios/:id', component: PromediosComponent}
];

// tslint:disable-next-line: eofline
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);