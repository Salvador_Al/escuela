import { Injectable } from '@angular/core' ;

@Injectable()

export class MateriasService {
    private Materias: Mat[] = [
        {
            clv_mat: '001',
            nom_mat: 'Int. a las ciencias',
            no_uni: '4',
        },
        {
            clv_mat: '002',
            nom_mat: 'Int. a la programacion',
            no_uni: '3',
        },
        {
            clv_mat: '003',
            nom_mat: 'Matematicas I',
            no_uni: '5',
        },
        {
            clv_mat: '004',
            nom_mat: 'Fisica',
            no_uni: '3',
        },
        {
            clv_mat: '005',
            nom_mat: 'Matematicas Basicas',
            no_uni: '3',
        },
        {
            clv_mat: '006',
            nom_mat: 'Etica',
            no_uni: '3',
        },
        {
            clv_mat: '007',
            nom_mat: 'Ecu. Diferenciales',
            no_uni: '3',
        },
        {
            clv_mat: '008',
            nom_mat: 'Simulacion',
            no_uni: '6',
        }
    ];

    getMat() {
        return this.Materias;
    }

    getMateria(clave: string) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.Materias.length; i++) {
            if (this.Materias[i].clv_mat === clave) {
                return this.Materias[i];
            }
        }
    }

    getNomMat(clave: string) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.Materias.length; i++) {
            if (this.Materias[i].clv_mat === clave) {
                return this.Materias[i].nom_mat;
            }
        }
    }

}

export interface Mat {
    clv_mat: string;
    nom_mat: string;
    no_uni: string;
// tslint:disable-next-line:eofline
}