import { Injectable } from '@angular/core' ;
import { MateriasService, Mat } from './Materias.service';

@Injectable()

export class AlumnosService {
    Materias: Mat [] = [];
    private Alumnos: Alu[] = [
        {
            nom_al: 'Adriana',
            no_ctrl: '42010',
            carrera: 'Lic. en Informatica',
            semestre: '1vo',
            c_materia: ['001', '005', '004', '007']
        },
        {
            nom_al: 'Patricia',
            no_ctrl: '42011',
            carrera: 'Lic. en Informatica',
            semestre: '1vo',
            c_materia: ['002', '005', '008']
        },
        {
            nom_al: 'Alma',
            no_ctrl: '42012',
            carrera: 'Lic. en Informatica',
            semestre: '1vo',
            c_materia: ['004', '005', '007', '008']
        },
        {
            nom_al: 'Marcos',
            no_ctrl: '42013',
            carrera: 'Lic. en Informatica',
            semestre: '1vo',
            c_materia: ['001', '002', '003', '005']
        },
        {
            nom_al: 'Carlos',
            no_ctrl: '42014',
            carrera: 'Lic. en Informatica',
            semestre: '1vo',
            c_materia: ['006', '008', '002']
        },
        {
            nom_al: 'Ruben',
            no_ctrl: '42015',
            carrera: 'Ing. Sistemas',
            semestre: '1vo',
            c_materia: ['002', '007', '005']
        },
        {
            nom_al: 'Maria',
            no_ctrl: '42016',
            carrera: 'Ing. Sistemas',
            semestre: '1vo',
            c_materia: ['002', '005', '007', '004']
        },
        {
            nom_al: 'Sonia',
            no_ctrl: '42017',
            carrera: 'Ing. Sistemas',
            semestre: '1vo',
            c_materia: ['002', '003', '004']
        },
        {
            nom_al: 'Edgar',
            no_ctrl: '42018',
            carrera: 'Ing. Sistemas',
            semestre: '1vo',
            c_materia: ['006', '002', '004']
        },
        {
            nom_al: 'Luis',
            no_ctrl: '42019',
            carrera: 'Ing. Sistemas',
            semestre: '1vo',
            c_materia: ['002', '003', '005']
        }
    ];

    getAlu() {
        return this.Alumnos;
    }

    getNuCtrl(num: string) {
        return this.Alumnos[num];
    }
}

export interface Alu {
    nom_al: string;
    no_ctrl: string;
    carrera: string;
    semestre: string;
    c_materia: string[];
// tslint:disable-next-line: eofline
}